<?php

$app->get('/user/{id}/movies', function ($request,$response) {
   try{
       $id  = $request->getAttribute('id');
       $con = $this->db;
       $sql = <<<SQL
            SELECT u.id_user, username, rate, m.id_movie, title
            FROM user u, user_movies um, movies m
            WHERE u.id_user = um.id_user AND um.id_movie = m.id_movie
           	AND u.id_user = :id
SQL;


       $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	   $values = array(':id' => $id);
       $pre->execute($values);

       $result = null;
       foreach ($pre as $row) {
           $result[] = $row;
       }
       if($result){
           return $response->withJson(array('status' => 'true','result'=>$result),200);
       }else{
           return $response->withJson(array('status' => 'Users Not Found'),422);
       }
              
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});


//CREATE RATE
$app->post('/user/{idU}/movie/{idM}', function ($request, $response) {
   
   try{
       $id  = $request->getAttribute('idU');
       $idM = $request->getAttribute('idM');

       $con = $this->db;
       $sql = "INSERT INTO `user_movies`(`id_user`, `id_movie`, `rate`) VALUES (:idU, :id, :rate)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':idU' => $id,
         ':id' => $idM,
         ':rate' => $request->getParam('rate') 
       );
       $result = $pre->execute($values);
      
       return $response->withJson(array('status' => 'Rate added'), 200);
       
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
});