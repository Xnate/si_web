<?php

//CREATE MOVIE
$app->post('/movie', function ($request, $response) {
   
   try{
       $con = $this->db;
       $sql = "INSERT INTO `movies`(`title`) VALUES (:title)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':title' => $request->getParam('title')
       );
       $result = $pre->execute($values);
       return $response->withJson(array('status' => 'Movie Created'), 200);
       
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
});

//READ MOVIE
$app->get('/movie/{id}', function ($request,$response) {
   try{
       $id = $request->getAttribute('id');
       $con = $this->db;
       $sql = "SELECT * FROM movies WHERE id_movie = :id";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':id' => $id);
       $pre->execute($values);
       $result = $pre->fetch();
       if($result){
           return $response->withJson(array('status' => 'true','result'=> $result),200);
       }else{
           return $response->withJson(array('status' => 'Movie Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});

$app->get('/movies/', function ($request,$response) {
   try{
       $con = $this->db;
       $sql = "SELECT * FROM movies";  
       $result = null;
       foreach ($con->query($sql) as $row) {
           $result[] = $row;
       }
       if($result){
           return $response->withJson(array('status' => 'true','result'=>$result),200);
       }else{
           return $response->withJson(array('status' => 'Movies Not Found'),422);
       }         
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});

//get movies by rating
$app->get('/movies/rating', function ($request,$response) {
   try{
      $con = $this->db;
      $sql = <<<SQL
        SELECT m.id_movie, m.title, AVG(rate)
        FROM movies m, user_movies um 
        WHERE m.id_movie = um.id_movie
        GROUP BY 1
        ORDER BY 3 DESC
SQL
;  
      $result = null;
      foreach ($con->query($sql) as $row) {
        $result[] = $row;
      }
      if($result){
        return $response->withJson(array('status' => 'true','result'=>$result),200);
      }else{
        return $response->withJson(array('status' => 'Movies Not Found'),422);
      }         
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
});

//UPDATE MOVIES

$app->put('/movie/{id}', function ($request,$response) {
   try{
       $id     = $request->getAttribute('id');
       $con = $this->db;
       $sql = "UPDATE movies SET title=:title WHERE id_movie = :id";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':title' => $request->getParam('title'),
                       ':id' => $request->getParam('id')
                  );

       $result =  $pre->execute($values);
       if($result){
           return $response->withJson(array('status' => 'Movie Updated'),200);
       }else{
           return $response->withJson(array('status' => 'Movie Not Found'),422);
       }
              
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

//DELETE MOVIE
$app->delete('/movie/{id}', function ($request,$response) {
   try{
       $id  = $request->getAttribute('id');
       $con = $this->db;
       $sql = "DELETE FROM movies WHERE id_movie = :id";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
       ':id' => $id);
       $result = $pre->execute($values);
       if($result){
           return $response->withJson(array('status' => 'Movie Deleted'),200);
       }else{
           return $response->withJson(array('status' => 'Movie Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

//GET MOVIES WITH A GENRE
$app->get('/movies/{genre}', function ($request, $response) {
   $genre = $request->getAttribute('genre');
    try{
       $con = $this->db;
       $sql = <<<SQL
          SELECT * 
          FROM movies m, movies_genre mg, genre g 
          WHERE m.id_movie = mg.id_movie
          AND mg.id_genre = g.id_genre
          AND g.libelle LIKE :genre 
SQL
; 
      $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
      $values = array(':genre' => $genre);
      $pre->execute($values);
      $result = null;
      foreach ($pre as $row) {
        $result[] = $row;
      }

      if($result){
        return $response->withJson(array('status' => 'true','result'=>$result),200);
      }else{
        return $response->withJson(array('status' => 'Movies Not Found'),422);
      }

   } catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});
/*
//GET MOVIES ORDERED BY RATING.
$app->get('/movies/filter/rating', function ($request, $response) {
    try{
       $con = $this->db;
       $sql = <<<SQL
          SELECT m.id_movie, title, AVG(rate)
          FROM movies m, user_movies um
          WHERE m.id_movie = um.id_movie
          GROUP BY id_movie
          ORDER BY 3 DESC
SQL
; 
      $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
      $pre->execute();
      $result = null;
      foreach ($pre as $row) {
        $result[] = $row;
      }

      if($result){
        return $response->withJson(array('status' => 'true','result'=>$result),200);
      }else{
        return $response->withJson(array('status' => 'Movies Not Found'),422);
      }

   } catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});
*/