<?php

$app->get('/user/{id}/friends', function ($request,$response) {
   try{
       $id  = $request->getAttribute('id');
       $con = $this->db;
       $sql = <<<SQL
            SELECT id_user, username, email 
            FROM user
            WHERE id_user IN (SELECT id_user2
                              FROM user_friends
                              WHERE id_user = :id)
SQL;

       $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':id' => $id);
       $pre->execute($values);
       $result = null;
       foreach ($pre as $row) {
           $result[] = $row;
       }

       if($result){
           return $response->withJson(array('status' => 'true','result'=>$result),200);
       }else{
           return $response->withJson(array('status' => 'Users Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

$app->get('/user/{id}/friend/{idF}', function ($request,$response) {
   try{
       $id  = $request->getAttribute('id');
       $idF = $request->getAttribute('idF');
       $con = $this->db;
       $sql = <<<SQL
            SELECT id_user, username, email 
            FROM user
            WHERE id_user IN (SELECT id_user2
                              FROM user_friends
                              WHERE id_user = :id AND id_user2 = :idF)
SQL;

       $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':id' => $id,
                       ':idF' => $idF);
       $pre->execute($values);
       $result = $pre->fetch();
       if($result) {
           return $response->withJson(array('status' => 'true','result'=> $result),200);
       }else {
           return $response->withJson(array('status' => 'Friend Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

//CREATE FRIEND
$app->post('/user/{idU}/friend', function ($request, $response) {
   
   try{
       $id  = $request->getAttribute('idU');
       $con = $this->db;
       $sql = "INSERT INTO `user_friends`(`id_user`, `id_user2`) VALUES (:idU, :id)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':idU' => $id,
         ':id' => $request->getParam('id')
       );
       $result = $pre->execute($values);
      
       //je truande parce que mysql + phpmyadmin est nul et/ou que je sais pas m'en servir
       $sql = "INSERT INTO `user_friends`(`id_user`, `id_user2`) VALUES (:idU, :id2)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':id2' => $id,
         ':idU' => $request->getParam('id')
       );
       $result = $pre->execute($values);
      

       return $response->withJson(array('status' => 'Friend added'), 200);
       
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
});

//DELETE FRIEND
$app->delete('/user/{idU}/friend/{idF}', function ($request, $response) {
   
   try{
       $id  = $request->getAttribute('idU');
       $idF = $request->getAttribute('idF');
       $con = $this->db;
       $sql = "DELETE FROM `user_friends` WHERE (id_user = :idU AND id_user2 = :idF) OR (id_user = :idF AND id_user2 = :idU)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':idU' => $id,
         ':idF' => $request->getParam('idF')
       );
       $result = $pre->execute($values);
       return $response->withJson(array('status' => 'Friend deleted'), 200);
       
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
});

