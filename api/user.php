<?php

$app->post('/user/login', function($request, $response) {
   try{
       $username  = $request->getParam('username');
       $password = $request->getParam('password');
       $con = $this->db;
       $sql = "SELECT username, password FROM user WHERE username = :username";
       $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':username' => $username);
                  //     ':password' => password_hash($password, PASSWORD_DEFAULT));
       $pre->execute($values);
       $result = $pre->fetch();

       if(password_verify($password, $result["password"]) ){
           return $response->withJson(array('status' => 'true','result'=> $result), 200);
       }else{
           return $response->withJson(array('status' => 'Invalid credentials'),422);
       }      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});

//CREATE USER
$app->post('/user', function ($request, $response) {
   
   try{
       $con = $this->db;
       $sql = "INSERT INTO `user`(`username`, `email`,`password`) VALUES (:username, :email, :password)";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
         ':username' => $request->getParam('username'),
         ':email' => $request->getParam('email'),
         ':password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT)
       );
       $result = $pre->execute($values);
       return $response->withJson(array('status' => 'User Created'), 200);
   }
   
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()), 422);
   }
   
});

//READ USER
$app->get('/user/{id}', function ($request,$response) {
   try{
       $id  = $request->getAttribute('id');
       $con = $this->db;
       $sql = "SELECT * FROM user WHERE id_user = :id";
       $pre = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':id' => $id);
       $pre->execute($values);
       $result = $pre->fetch();
       if($result){
           return $response->withJson(array('status' => 'true','result'=> $result),200);
       }else{
           return $response->withJson(array('status' => 'User Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

$app->get('/users', function ($request,$response) {
   try{
       $con = $this->db;
       $sql = "SELECT * FROM user";  
       $result = null;
       foreach ($con->query($sql) as $row) {
           $result[] = $row;
       }
       if($result){
           return $response->withJson(array('status' => 'true','result'=>$result),200);
       }else{
           return $response->withJson(array('status' => 'Users Not Found'),422);
       }
              
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

//UPDATE USER

$app->put('/user/{id}', function ($request,$response) {
   try{
       $id     = $request->getAttribute('id');
       $con = $this->db;
       $sql = "UPDATE user SET username=:username, email=:email, password=:password WHERE id_user = :id";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(':username' => $request->getParam('username'),
                       ':email' => $request->getParam('email'),
                       ':password' => password_hash($request->getParam('password'),PASSWORD_DEFAULT),
                       ':id' => $id
       );

       $result =  $pre->execute($values);
       if($result){
           return $response->withJson(array('status' => 'User Updated'),200);
       }else{
           return $response->withJson(array('status' => 'User Not Found'),422);
       }
              
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});

//DELETE USER
$app->delete('/user/{id}', function ($request,$response) {
   try{
       $id     = $request->getAttribute('id');
       $con = $this->db;
       $sql = "DELETE FROM user WHERE id_user = :id";
       $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $values = array(
       ':id' => $id);
       $result = $pre->execute($values);
       if($result){
           return $response->withJson(array('status' => 'User Deleted'),200);
       }else{
           return $response->withJson(array('status' => 'User Not Found'),422);
       }
      
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
   
});