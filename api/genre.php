<?php

//GET GENRES
$app->get('/genres', function ($request,$response) {
   try{
       $con = $this->db;
       $sql = "SELECT * FROM genre";  
       $result = null;
       foreach ($con->query($sql) as $row) {
           $result[] = $row;
       }
       if($result){
           return $response->withJson(array('status' => 'true','result'=>$result),200);
       }else{
           return $response->withJson(array('status' => 'Movies Not Found'),422);
       }         
   }
   catch(\Exception $ex){
       return $response->withJson(array('error' => $ex->getMessage()),422);
   }
});